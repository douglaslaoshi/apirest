package com.prod.apirest.controller;

import com.prod.apirest.com.prod.apirest.models.Produto;
import com.prod.apirest.repo.ProdutoRepo;
import com.prod.apirest.view.RelatorioCsv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class Produtoweb {

	
	@Autowired
	private RelatorioCsv cs;
	
	@RequestMapping(path="/produtos")
	public void setDataInDB() {
		cs.salvaRelatorioCsv();
	}
	
   
	@Autowired
    ProdutoRepo produtoRepo;

    @GetMapping("/produtos")
    public ResponseEntity<?> getProdutos() {
        List<Produto> produtoList = produtoRepo.findAll(); 

        
        return ResponseEntity.status(HttpStatus.OK).body(produtoList);
            
    }
    
    
    @GetMapping("produtos/{id}")
    public Produto listaProdutoUnico(@PathVariable(value = "id") long id) {
    
        return produtoRepo.findById(id);
            
    }
   

    

    
    @PostMapping
    public ResponseEntity<?> addProduto(@RequestBody Produto body) {
        produtoRepo.save(body);

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }


    @PutMapping
    public ResponseEntity<?> updateProduto(@RequestBody Produto body) {
        produtoRepo.save(body);

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }
    
    @DeleteMapping
    public ResponseEntity<?> deleteProduto(@RequestBody Produto body) {
        produtoRepo.delete(body);

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }   
    
    
    
	
	
    
    
    
 
     
    }
   

