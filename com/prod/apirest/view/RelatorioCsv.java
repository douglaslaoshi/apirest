package com.prod.apirest.view;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prod.apirest.com.prod.apirest.models.Produto;
import com.prod.apirest.repo.ProdutoRepo;

@Service

public class RelatorioCsv {

	
	@Autowired
	private ProdutoRepo relat;
	String  line="";
	
public void salvaRelatorioCsv() {
	
	try {
		BufferedReader br=new BufferedReader(new FileReader("src/main/resources/Produtos.csv"));
		while ((line=br.readLine())!=null) {
			String [] data=line.split(",");
			Produto c=new Produto();
		
			c.setNome(data[0]);
			c.setQuantidade(data[1]);
			c.setValor(data[2]);
			c.setValidade(data[3]);
			relat.save(c);
		}
	}catch (IOException e) {
		e.printStackTrace();
	}
}
	
	
}


